from conans import ConanFile, CMake, tools

class GrpcConan(ConanFile):
    name = "gRPC"
    version = "1.10.0"
    license = "TODO"
    url = "TODO"
    description = "gRPC - An RPC library and framework"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False", "zlib:shared=False", "c-ares:shared=False"
    generators = "cmake"
    requires = (("protobuf/[~=3.5.0]@thirdparty/stable"),
        ("zlib/[~=1.2.11]@conan/stable"),
        ("OpenSSL/1.1.0g@conan/stable"),
        ("c-ares/[~=1.14.0]@thirdparty/stable"))

    def source(self):
        self.run("git clone --recurse-submodules https://github.com/grpc/grpc")
        tag = "v" + self.version
        self.run("cd grpc && git checkout " + tag)

        tools.replace_in_file("grpc/CMakeLists.txt", "project(${PACKAGE_NAME} C CXX)", '''project(${PACKAGE_NAME} C CXX)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')

        # add correct library variable for OpenSSL
        if self.settings.os == "Windows":
            tools.replace_in_file("grpc/cmake/ssl.cmake", "set(_gRPC_SSL_LIBRARIES OpenSSL::SSL OpenSSL::Crypto)", '''set(_gRPC_SSL_LIBRARIES OpenSSL::SSL OpenSSL::Crypto crypt32)''')

    def build(self):            
        cmake = CMake(self)
        cmake.definitions["gRPC_PROTOBUF_PROVIDER"] = "package"
        cmake.definitions["gRPC_ZLIB_PROVIDER"] = "package"
        cmake.definitions["gRPC_CARES_PROVIDER"] = "package"
        cmake.definitions["gRPC_SSL_PROVIDER"] = "package"
        cmake.definitions["gRPC_MSVC_STATIC_RUNTIME"] = "OFF"
        cmake.configure(source_folder="grpc")
        cmake.build()
        cmake.install()

    def package_info(self):
        self.cpp_info.bindirs = ["bin"]
        self.cpp_info.includedirs = ["include"]
        self.cpp_info.libdirs = ["lib"]

        self.cpp_info.libs = [
            "gpr",
            "grpc",
            "grpc++",
            "grpc++_reflection"
        ]

        if self.settings.os == "Windows":
            self.cpp_info.defines.append("_WIN32_WINNT=0x600")
