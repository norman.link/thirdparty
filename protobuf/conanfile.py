from conans import ConanFile, CMake, tools

class ProtobufConan(ConanFile):
    name = "protobuf"
    version = "3.5.2"
    license = "TODO"
    url = "TODO"
    description = "Protocol Buffers - Google's data interchange format "
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=False"
    generators = "cmake"

    def source(self):
        self.run("git clone https://github.com/google/protobuf.git")
        tag = "v" + self.version
        self.run("cd protobuf && git checkout " + tag)

        tools.replace_in_file("protobuf/cmake/CMakeLists.txt", "project(protobuf C CXX)", '''project(protobuf C CXX)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')

    def build(self):
        cmake = CMake(self)
        cmake.definitions["protobuf_BUILD_TESTS"] = "OFF"
        cmake.definitions["protobuf_MSVC_STATIC_RUNTIME"] = "OFF"
        cmake.configure(source_folder="protobuf/cmake")
        cmake.build()
        cmake.install()

    def package_info(self):
        self.cpp_info.bindirs = ["bin"]
        self.cpp_info.includedirs = ["include"]
        self.cpp_info.libdirs = ["lib"]

        libprotobuf = "libprotobuf"
        if self.settings.build_type == "Debug":
            libprotobuf += "d"

        self.cpp_info.libs = [libprotobuf]
