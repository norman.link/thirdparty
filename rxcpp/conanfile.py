#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile, tools
import os

class RxcppConan(ConanFile):
    name = "rxcpp"
    version = "master"
    url = "TODO"
    description = "Library for composing operations on streams of asynchronous events."
    license = "Apache-2.0"
    exports = ["LICENSE.md"]
    root = "RxCpp"

    def source(self):
        self.run("git clone https://github.com/Reactive-Extensions/RxCpp.git")
        self.run("cd RxCpp && git checkout master")

    def package(self):
        header_dir = os.path.join(self.root, "Rx", "v2", "src")
        self.copy(pattern="*", dst="include", src=header_dir)

    def package_id(self):
        self.info.header_only()
