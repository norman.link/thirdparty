from conans import ConanFile, CMake, tools

class GrpcConan(ConanFile):
    name = "c-ares"
    version = "1.14.0"
    license = "MIT"
    url = "TODO"
    description = "A C library for asynchronous DNS requests"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = "shared=True"
    generators = "cmake"

    def source(self):
        self.run("git clone --recurse-submodules https://github.com/c-ares/c-ares.git cares")
        tag = "cares-" + self.version.replace(".", "_")
        self.run("cd cares && git checkout " + tag)

        tools.replace_in_file("cares/CMakeLists.txt", "PROJECT (c-ares C)", '''PROJECT (c-ares C)
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()''')

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CARES_STATIC"] = not self.options.shared
        cmake.definitions["CARES_SHARED"] = self.options.shared
        cmake.definitions["CARES_INSTALL"] = "ON"
        cmake.configure(source_folder="cares")
        cmake.build()
        cmake.install()

    def package_info(self):
        self.cpp_info.bindirs = ["bin"]
        self.cpp_info.includedirs = ["include"]
        self.cpp_info.libdirs = ["lib"]

        self.cpp_info.libs = ["cares"]
